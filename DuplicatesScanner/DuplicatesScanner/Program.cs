﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplicatesScanner
{
    class Program
    {
        static void Main(string[] args)
        {
            new ScannerManager().RunTask();
        }
    }
}
