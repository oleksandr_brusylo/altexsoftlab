﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplicatesScanner
{
    class ScannerManager
    {
        private string userInput;
        private FileTreeBuilder builder;
        private FileComparator comparator;
        public void RunTask()
        {
            while (true)
            {
                Console.WriteLine("Enter directory path to scan for duplicates");
                if (UserInputIsDirectory())
                {
                    builder = new FileTreeBuilder(userInput);
                    builder.BuildFilesTree();                    
                    var possibleDuplicates = builder.GetPossibleDuplicates();
                    comparator = new FileComparator(possibleDuplicates);
                    comparator.ProcessPossibleDuplicates();                    
                }
                else
                {
                    Console.WriteLine("!!!Incorrect directory path!!!");
                }                
            }
        }

        private bool UserInputIsDirectory()
        {
            userInput = Console.ReadLine();
            return Directory.Exists(userInput);               
        }

    }
}
