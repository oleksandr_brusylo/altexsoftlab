﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplicatesScanner
{
    class FileTreeBuilder
    {
        private Dictionary<long, List<FileInfo>> inputFiles = new Dictionary<long, List<FileInfo>>();
        private string rootPath;
        private Queue<DirectoryInfo> directoriesQueue = new Queue<DirectoryInfo>();
        private LinkedList<List<FileInfo>> possibleDuplicates = new LinkedList<List<FileInfo>>();

        public LinkedList<List<FileInfo>> GetPossibleDuplicates ()
        {
            return possibleDuplicates;
        }

        public FileTreeBuilder(string path)
        {
            rootPath = path;
        }

        public void BuildFilesTree()
        {
            DirectoryInfo root = new DirectoryInfo(rootPath);            
            directoriesQueue.Enqueue(root);
            SortFilesInDirecotry(root);
            while (directoriesQueue.Count != 0)
            {                
                DirectoryInfo[] directoriesList = directoriesQueue.Dequeue().GetDirectories();                
                foreach (DirectoryInfo currentDirectory in directoriesList)
                {
                    directoriesQueue.Enqueue(currentDirectory);                    
                    SortFilesInDirecotry(currentDirectory);
                }
            }
            //ShowResult();
            PrepareFilesQueue();
        }

        private void PrepareFilesQueue()
        {
            foreach (KeyValuePair<long, List<FileInfo>> kvp in inputFiles)
            {
                List<FileInfo> list = kvp.Value;
                if (list.Count > 1)
                {
                    possibleDuplicates.AddFirst(list);
                }
            }
            Console.WriteLine("Files queue ready ! \nSize: {0}", possibleDuplicates.Count);
        }

        private void ShowResult()
        {

            foreach (long size in inputFiles.Keys) {            
                foreach (FileInfo file in inputFiles[size])
                {
                    Console.WriteLine("File size: {0} File name: {1}", size, file.FullName);
                }
            }
        }

        private void SortFilesInDirecotry(DirectoryInfo currentDirectory)
        {
            FileInfo[] files = currentDirectory.GetFiles();
            foreach (FileInfo file in files)
            {
                FilterFileBySize(file);
            }
        }

        private void FilterFileBySize(FileInfo file)
        {
            long fileLength = file.Length;
            if (fileLength == 0)
            {
                return;
            }

            if (inputFiles.ContainsKey(fileLength))
            {
                List<FileInfo> list = inputFiles[fileLength];
                list.Add(file);
            }
            else
            {
                List<FileInfo> list = new List<FileInfo>();
                list.Add(file);
                inputFiles.Add(fileLength, list);
            }
        }

    }
}
