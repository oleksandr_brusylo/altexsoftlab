﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplicatesScanner
{
    class FileComparator
    {
        private LinkedList<List<FileInfo>> possibleDuplicates;
        private int bufferSize = 4 * 1024;

        public FileComparator(LinkedList<List<FileInfo>> possibleDuplicates)
        {
            this.possibleDuplicates = possibleDuplicates;
        }

        public void ProcessPossibleDuplicates ()
        {
            foreach (List<FileInfo> sameSizeFiles in possibleDuplicates)
            {
                CompareFilesForIdentity(sameSizeFiles);
            }
        }

        private void CompareFilesForIdentity(List<FileInfo> sameSizeFiles)
        {
            FileInfo possibleDuplicateOne;
            FileInfo possibleDuplicateTwo;

            for (int i = 0; i < sameSizeFiles.Count - 1; i++)
            {
                for (int j = i+1; j < sameSizeFiles.Count; j++)
                {
                    possibleDuplicateOne = sameSizeFiles[i];
                    possibleDuplicateTwo = sameSizeFiles[j];                    
                    ReadFilesContent(possibleDuplicateOne, possibleDuplicateTwo);
                }

            }
        }
        

        

        public object GetDuplicatesList ()
        {            
            return new object(); //Result
        }

        private bool CompareBuffers(byte[] arrayOne, byte[] arrayTwo)
        {
            return arrayOne.SequenceEqual(arrayTwo);
        }

        private bool ReadFilesContent(FileInfo fileOne, FileInfo fileTwo)
        {
            byte[] bufferOne = new byte[bufferSize];
            byte[] bufferTwo = new byte[bufferSize];
            var streamOne = fileOne.OpenRead();
            var streamTwo = fileTwo.OpenRead();

            long fileSize = fileOne.Length;
            long bytesCompared = 0;
            long bytesReaded;
            while (bytesCompared != fileSize)
            {
                bytesReaded = streamOne.Read(bufferOne, 0, bufferSize);
                streamTwo.Read(bufferTwo, 0, bufferSize);
                bytesCompared += bytesReaded;
                if (!CompareBuffers(bufferOne, bufferTwo))
                {
                    streamOne.Close();
                    streamTwo.Close();
                    Console.WriteLine("Not a duplicates: {0} and {1} ! Size: {2}", fileOne, fileTwo, fileSize);
                    return false; 
                }
            }
            streamOne.Close();
            streamTwo.Close();
            Console.WriteLine("Duplicates: {0} and {1} ! Size: {2}", fileOne, fileTwo, fileSize);
            return true;      
        }
    }
}

 