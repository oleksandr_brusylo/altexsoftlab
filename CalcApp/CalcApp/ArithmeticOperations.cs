﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    static class ArithmeticOperations
    {
        private static Dictionary<string, Func<dynamic, dynamic, dynamic>> availableOperations = new Dictionary<string, Func<dynamic, dynamic, dynamic>>
    {
        { "+", (firstNumber, secondNumber) => firstNumber + secondNumber },
        { "-", (firstNumber, secondNumber) => firstNumber - secondNumber },
        { "*", (firstNumber, secondNumber) => firstNumber * secondNumber },
        { "/", (firstNumber, secondNumber) => firstNumber / secondNumber }
    };
        public static Dictionary<string, Func<dynamic, dynamic, dynamic>> AvailableOperations
        {
            get
            {
                return availableOperations;
            }
        }
        public static bool OperationIsValid (string userInput)
        {             
            return availableOperations.ContainsKey(userInput); ;
        }
    }
}
