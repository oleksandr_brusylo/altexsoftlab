﻿using CalcApp.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    class CalcManager
    {
        private IUserInput userInput;
        private Calculator calc;

        public void RunTask()
        {
            ShowInfo();
            while (true)
            {
                try
                {
                    userInput = new ConsoleUserInput();
                    userInput.ReadUserInput();
                    calc = new Calculator(userInput.GetFirstNumber, userInput.GetSecondNumber);
                    calc.Calculate(userInput.GetOperation);
                }
                catch (FormatException)
                {
                    Console.WriteLine(Resources.ResourceManager.GetString("FormatException"));
                }
                catch (ArgumentException)
                {
                    Console.WriteLine(Resources.ResourceManager.GetString("ArgumentException"));
                }
                catch (DivideByZeroException)
                {
                    Console.WriteLine("!!!Division by zero is not allowed!!!");
                }
            }
        }

        private void ShowInfo()
        {
            Console.WriteLine("This is simple application which allows You to calculate value of two numbers");
            Console.WriteLine("Available arithmetic operations: ");
            foreach (string operation in ArithmeticOperations.AvailableOperations.Keys)
            {
                Console.Write(operation + " ");
            }
            Console.WriteLine("");
        }
    }
}
