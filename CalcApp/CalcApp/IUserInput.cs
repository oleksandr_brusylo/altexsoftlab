﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    interface IUserInput
    {
        void ReadUserInput();
        Number GetFirstNumber { get; }
        Number GetSecondNumber { get; }
        string GetOperation { get; }               
    }
}
