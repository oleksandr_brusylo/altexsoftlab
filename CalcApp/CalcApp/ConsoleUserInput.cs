﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    class ConsoleUserInput : IUserInput
    {
        private Number first;
        private Number second;
        private string arithmeticOperation;

        public void ReadUserInput()

        {
            first = DetermineNumberType();
            second = (first.GetType() == typeof(ComplexNumber)) ? new ComplexNumber() : new Number();
            first.SetValue();
            DetermineArithmenticOperation();
            second.SetValue();
        }

        private Number DetermineNumberType()
        {
            Console.WriteLine("Type '1' for calculate simple numbers");
            Console.WriteLine("Type '2' for calculate complex numbers");
            int numberType = Convert.ToInt32(Console.ReadLine());
            if (numberType == 1)
            {
                Console.WriteLine("Simple Number Choosed");
                return new Number();
            }
            else if (numberType == 2)
            {
                Console.WriteLine("Complex Number Choosed");
                return new ComplexNumber();
            }
            throw new FormatException();
        }

        private void DetermineArithmenticOperation()
        {
            Console.WriteLine("Enter arithmetic operation");
            arithmeticOperation = Console.ReadLine();
            if (!ArithmeticOperations.OperationIsValid(arithmeticOperation))
                throw new ArgumentException();
        }

        public Number GetFirstNumber
        {
            get
            {
                return first;
            }
        }

        public Number GetSecondNumber
        {
            get
            {
                return second;
            }
        }

        public string GetOperation
        {
            get
            {
                return arithmeticOperation;
            }
        }
    }
}
