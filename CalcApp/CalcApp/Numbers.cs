﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    class Number
    {
        private double value;

        public Number()
        {}

        public Number(double value)
        {
            this.value = value;
        }

        public virtual void SetValue()
        {
            Console.WriteLine("Enter number");
            value = Convert.ToDouble(Console.ReadLine());
        }

        public override string ToString()
        {
            return Convert.ToString(Math.Round(value, 2));
        }

        public static Number operator +(Number first, Number second)
        {
            return new Number(first.Value + second.Value);
        }

        public static Number operator -(Number first, Number second)
        {
            return new Number(first.Value - second.Value);
        }

        public static Number operator *(Number first, Number second)
        {
            return new Number(first.Value * second.Value);
        }

        public static Number operator /(Number first, Number second)
        {
            return new Number(first.Value / second.Value);
        }

        public double Value
        {
            get
            {
                return value;
            }            
        }

    }
}
