﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    interface UserInput
    {
        void ReadUserInput();
        string ArithmeticOperation { get; set; }
        double FirstNumber { get; set; }
        double SecondNumber { get; set; }
    }
}
