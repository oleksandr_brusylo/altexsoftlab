﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    class Calculator
    {
        private dynamic firstNumber;
        private dynamic secondNumber;      

        public Calculator(dynamic firstNumber, dynamic secondNumber)
        {                  
            this.firstNumber = firstNumber;
            this.secondNumber = secondNumber;
        }

        public string Calculate(string operation)
        {            
            string result = Convert.ToString(ArithmeticOperations.AvailableOperations[operation](firstNumber, secondNumber));
            Console.WriteLine("Result: " + result);
            return result;
        }

    }
}

