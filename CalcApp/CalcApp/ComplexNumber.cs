﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcApp
{
    class ComplexNumber : Number
    {
        private double realValue;
        private double imaginaryValue;

        public ComplexNumber()
        {}

        public ComplexNumber(double real, double imaginary)
        {
            realValue = real;
            imaginaryValue = imaginary;
        }

        public override void SetValue()
        {
            Console.WriteLine("Enter real part of complex number");
            realValue = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter imaginary part of complex number");
            imaginaryValue = Convert.ToDouble(Console.ReadLine());
        }

        public static ComplexNumber operator +(ComplexNumber first, ComplexNumber second)
        {
            return new ComplexNumber(first.realValue + second.realValue, 
                                     first.imaginaryValue + second.imaginaryValue);
        }

        public static ComplexNumber operator -(ComplexNumber first, ComplexNumber second)
        {
            return new ComplexNumber(first.realValue - second.realValue, 
                                     first.imaginaryValue - second.imaginaryValue);
        }

        public static ComplexNumber operator *(ComplexNumber first, ComplexNumber second)
        {   
            return new ComplexNumber(first.realValue * second.realValue - first.imaginaryValue * second.imaginaryValue, 
                                     first.realValue * second.imaginaryValue + first.imaginaryValue * second.realValue);
        }

        public static ComplexNumber operator /(ComplexNumber first, ComplexNumber second)
        {   
            return new ComplexNumber((first.realValue * second.realValue + first.imaginaryValue * second.imaginaryValue)
                                     /(Math.Pow(second.realValue, 2) + Math.Pow(second.imaginaryValue, 2)), 
                                     (first.imaginaryValue * second.realValue - first.realValue * second.imaginaryValue)
                                     /(Math.Pow(second.realValue, 2) + Math.Pow(second.imaginaryValue, 2)));
        }        

        public override string ToString()
        {
            if (imaginaryValue < 0)
            {
                return (String.Format("{0} - {1}j", Math.Round(realValue, 2), Math.Round(Math.Abs(imaginaryValue),2)));
            }
            return (String.Format("{0} + {1}j", Math.Round(realValue, 2), Math.Round(imaginaryValue, 2)));
        }
    }
}
