﻿using StringExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneNumberRegExp
{
    class Manager
    {
        internal void DoWork()
        {
            Console.WriteLine("Enter text to define is it phone number in format XXX-XXX-XX-XX");
            string input = Console.ReadLine();
            Console.WriteLine(input.IsStringPhoneNumber());
            Console.ReadKey();
        }
    }
}
