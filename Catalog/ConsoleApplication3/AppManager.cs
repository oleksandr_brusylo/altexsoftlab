﻿using System;

namespace Catalog
{
    class AppManager
    {
             
        private InputHandler inputHandler;

        public AppManager()
        {                        
            inputHandler = new InputHandler();
        }

        public void RunTask()
        {            
            inputHandler.CatalogChanged += OnCatalogChanged; 
            inputHandler.HandleInput();
        }

        private void OnCatalogChanged(object sender, CatalogEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

    }
}
