﻿using Catalog.Properties;
using System;
using System.Collections.Generic;
using System.IO;

namespace Catalog
{
    internal class InputHandler
    {
        public event EventHandler<CatalogEventArgs> CatalogChanged;
        private ConsoleUserInput consoleUserInput;
        private List<MovieEntity> entities;
        private string info = Resources.ResourceManager.GetString("Info");
        private string sortingOptions = Resources.ResourceManager.GetString("Sorting_options");
        private string defaultSorting = Resources.ResourceManager.GetString("Default_sorting");
        private readonly string XML = "xml";
        private readonly string BIN = "bin";

        public InputHandler()
        {
            consoleUserInput = new ConsoleUserInput();
            entities = new List<MovieEntity>();
        }

        protected virtual void OnCatalogChanged(string message)
        {
            if (CatalogChanged != null)
                CatalogChanged(entities, new CatalogEventArgs(message));
        }

        internal void HandleInput()
        {
            ShowAppInfo();
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "list":
                        ShowEntities();
                        break;
                    case "add":
                        AddEntity();
                        break;
                    case "sort":
                        SortEntities();
                        break;
                    case "help":
                        ShowAppInfo();
                        break;
                    case "save":
                        SaveEntities(XML);
                        break;
                    case "load":
                        LoadEntities(XML);
                        break;
                    case "clear":
                        DeleteEntities();
                        break;
                    case "exit":
                        return;
                    default:
                        Console.WriteLine("There are no such operation !");
                        break;
                }
            }
        }


        private void ShowEntities()
        {
            if (entities.Count == 0)
            {
                Console.WriteLine("There are no entities !");
            }
            else
            {
                string separator = new string('-', 50);
                int entityCounter = 1;
                foreach (MovieEntity movie in entities)
                {
                    Console.WriteLine(separator);
                    Console.WriteLine("№ {0}", entityCounter++);
                    Console.WriteLine("Title: {0}", movie.Title);
                    Console.WriteLine("Release Year: {0}", movie.ReleaseYear);
                    Console.WriteLine("ID: {0}", movie.MovieId);
                }
                Console.WriteLine(separator);
            }
        }

        private void AddEntity()
        {
            consoleUserInput.ReadUserInput();
            entities.Add(consoleUserInput.GetMovieEntity());
            OnCatalogChanged("Movie Entity Added");

        }

        private void SortEntities()
        {
            if (entities.Count >= 2)
            {
                entities.Sort(DefineSortingOrder());
                OnCatalogChanged("Entities sorted");
                ShowEntities();
            }
            else
            {
                ShowEntities();
            }
        }

        private void ShowAppInfo()
        {
            Console.WriteLine(info);
        }

        private void SaveEntities(string argument)
        {
            var saver = new MovieSerializer<MovieEntity>();
            switch (argument)
            {
                case "xml":
                    saver.SaveDataXml(entities);
                    break;
                case "bin":
                    saver.SaveDataBin(entities);
                    break;
                default:
                    break;
            }
            OnCatalogChanged("Movie Entities saved");
        }

        private void LoadEntities(string argument)
        {
            var loader = new MovieSerializer<MovieEntity>();
            List<MovieEntity> list;
            switch (argument)
            {
                case "xml":
                    loader.LoadXmlData();
                    break;
                case "bin":
                    loader.LoadBinData();
                    break;
                default:
                    break;
            }
            list = loader.List;
            if (list != null)
            {
                entities = list;
                OnCatalogChanged("Movie Entities Loaded");
            }
            else
            {
                Console.WriteLine("Nothing to load");
            }
        }

        private void DeleteEntities()
        {
            entities.Clear();
            MovieEntity.InstanceCounter = 1;
            if (File.Exists("MyEntities.xml"))
            {
                File.Delete("MyEntities.xml");
                OnCatalogChanged("Entities deleted");
            }
            else
            {
                Console.WriteLine("Entities do not exist");
            }
        }

        private IComparer<MovieEntity> DefineSortingOrder()
        {

            Console.WriteLine(sortingOptions);

            switch (Console.ReadLine())
            {
                case "1":
                    return MovieEntitiesSorter.SortByYearAsc();
                case "2":
                    return MovieEntitiesSorter.SortByYearDesc();
                case "3":
                    return MovieEntitiesSorter.SortByTitleAsc();
                case "4":
                    return MovieEntitiesSorter.SortByTitleDesc();
                default:
                    Console.WriteLine(defaultSorting);
                    return MovieEntitiesSorter.SortByDefault();
            }
        }

    }
}