﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog
{
    class MovieEntitiesSorter
    {
        internal static IComparer<MovieEntity> SortByYearAsc()
        {
            return new SortEntitiesByYearAscending();
        }

        internal static IComparer<MovieEntity> SortByYearDesc()
        {
            return new SortEntitiesByYearDescending();
        }

        internal static IComparer<MovieEntity> SortByTitleAsc()
        {
            return new SortEntitiesByTitleAscending();
        }

        internal static IComparer<MovieEntity> SortByTitleDesc()
        {
            return new SortEntitiesByTitleDescending();
        }

        internal static IComparer<MovieEntity> SortByDefault()
        {
            return new SortEntitiesById();
        }

        private class SortEntitiesByYearAscending : IComparer<MovieEntity>
        {
            int IComparer<MovieEntity>.Compare(MovieEntity x, MovieEntity y)
            {
                int firstYear = Convert.ToInt32(x.ReleaseYear);
                int secondYear = Convert.ToInt32(y.ReleaseYear);
                if (firstYear == secondYear)
                {
                    return 0;
                }
                return firstYear > secondYear ? 1 : -1;

            }
        }

        private class SortEntitiesByYearDescending : IComparer<MovieEntity>
        {
            int IComparer<MovieEntity>.Compare(MovieEntity x, MovieEntity y)
            {
                int firstYear = Convert.ToInt32(x.ReleaseYear);
                int secondYear = Convert.ToInt32(y.ReleaseYear);
                if (firstYear == secondYear)
                {
                    return 0;
                }
                return firstYear < secondYear ? 1 : -1;

            }
        }

        private class SortEntitiesByTitleAscending : IComparer<MovieEntity>
        {
            public int Compare(MovieEntity x, MovieEntity y)
            {
                return x.Title.CompareTo(y.Title);
            }
        }

        private class SortEntitiesByTitleDescending : IComparer<MovieEntity>
        {
            int IComparer<MovieEntity>.Compare(MovieEntity x, MovieEntity y)
            {
                return (x.Title.CompareTo(y.Title))*(-1);
            }
        }

        private class SortEntitiesById : IComparer<MovieEntity>
        {
            public int Compare(MovieEntity x, MovieEntity y)
            {
                return x.MovieId > y.MovieId ? 1 : -1;
            }
        }

    }
}
