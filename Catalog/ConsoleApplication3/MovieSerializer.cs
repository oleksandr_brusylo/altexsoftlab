﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Catalog
{
    class MovieSerializer<T>
    {
        private List<T> list;

        public List<T> List
        {
            get { return list; }
        }

        internal void SaveDataBin(List<T> entities)
        {
            using (Stream stream = File.Open("MyEntities.bin", FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, entities);
            }
        }

        internal void SaveDataXml(List<T> entities)
        {
            using (Stream stream = File.Open("MyEntities.xml", FileMode.Create))
            {
                XmlSerializer xml = new XmlSerializer(entities.GetType());
                xml.Serialize(stream, entities);
            }
        }

        internal void LoadBinData()
        {
            if (File.Exists("MyEntities.bin"))
            {
                using (Stream stream = File.Open("MyEntities.bin", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    list = bin.Deserialize(stream) as List<T>;
                }
            }
        }

        internal void LoadXmlData()
        {
            if (File.Exists("MyEntities.xml"))
            {
                using (Stream stream = File.Open("MyEntities.xml", FileMode.Open))
                {
                    XmlSerializer xml = new XmlSerializer(typeof(List<T>));
                    list = xml.Deserialize(stream) as List<T>;
                }
            }
        }
    }
}
