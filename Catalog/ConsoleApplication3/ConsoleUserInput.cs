﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog
{
    class ConsoleUserInput : IUserInput
    {
        private MovieEntity movieEntity;
        private string movieTitle;
        private string movieReleaseYear;        

        public void ReadUserInput()
        {
            Console.WriteLine("Enter Movie Title");
            movieTitle = Console.ReadLine();
            Console.WriteLine("Enter Movie Release Year");
            movieReleaseYear = Console.ReadLine();            
            InitializeEntity();
            
        }

        private void InitializeEntity()
        {
            movieEntity = new MovieEntity();
            movieEntity.Title = movieTitle;
            movieEntity.ReleaseYear = movieReleaseYear;            
        }

        public MovieEntity GetMovieEntity()
        {
            return movieEntity;
        }       
    }
}
