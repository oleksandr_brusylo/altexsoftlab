﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Catalog
{
    [Serializable]
    public class MovieEntity : IComparable<MovieEntity>
    {
        private static int instanceCounter = 1;
        private int movieId;
        private string movieTitle;
        private string movieReleaseYear;

        public MovieEntity()
        {
            movieId = instanceCounter++;
        }

        public MovieEntity(string title, string releaseYear, int id)
        {
            movieTitle = title;
            movieReleaseYear = releaseYear;
            movieId = id;
        }

        public static int InstanceCounter
        {
            get { return instanceCounter; }
            set { instanceCounter = value; }
        }

        public string Title
        {
            get { return movieTitle; }
            set { movieTitle = value; }
        }

        public string ReleaseYear
        {
            get { return movieReleaseYear; }
            set { movieReleaseYear = value; }
        }

        public int MovieId
        {
            get { return movieId; }
            set { movieId = value; }
        }

        public int CompareTo(MovieEntity otherEntity)
        {
            if (otherEntity == null)
            {
                return 1;
            }
            return movieTitle.CompareTo(otherEntity.movieTitle);
        }

    }
}
