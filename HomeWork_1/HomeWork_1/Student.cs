﻿using System;

namespace HomeWork_1
{
    class Student
    {
        public string Name { get; set; }
        public string HomeAddress { get; set; }
        public int BirthYear { get; set; }
        public string SchoolFinished { get; set; }
    }
}
