﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork_1
{
    class AppManager
    {
        private IList<Student> _studentsList;

        public void RunTask()
        {
            _studentsList = new List<Student>
            {
                new Student {BirthYear = 1997, HomeAddress = "Address 1", Name = "Alex", SchoolFinished = "School 30"},
                new Student {BirthYear = 1996, HomeAddress = "Address 2", Name = "Bob", SchoolFinished = "School 1"},
                new Student {BirthYear = 1995, HomeAddress = "Address 3", Name = "Chris", SchoolFinished = "School 5"},
                new Student {BirthYear = 1993, HomeAddress = "Address 4", Name = "John", SchoolFinished = "School 4"},
                new Student {BirthYear = 1989, HomeAddress = "Address 5", Name = "Mark", SchoolFinished = "School 25"},
                new Student {BirthYear = 1990, HomeAddress = "Address 6", Name = "Scott", SchoolFinished = "School 1"}
            };

            ShowStudents();

            Console.WriteLine("Specify school for displaying students list");

            DisplayInfo();

        }

        private void ShowStudents()
        {
            foreach (var student in _studentsList)
            {
                Console.WriteLine(student.Name);
                Console.WriteLine(student.HomeAddress);
                Console.WriteLine(student.BirthYear);
                Console.WriteLine(student.SchoolFinished);
                Console.WriteLine(new string('=', 25));
            }
        }

        public void DisplayInfo()
        {
            var schoolName = Console.ReadLine();
            var result = from s in _studentsList
                         where string.Equals(s.SchoolFinished, schoolName, StringComparison.InvariantCultureIgnoreCase)
                         orderby s.BirthYear
                         select s;

            if (!result.Any())
            {
                Console.WriteLine("No students from specified school");
                return;
            }

            foreach (var student in result)
            {
                Console.WriteLine(student.Name);
                Console.WriteLine(student.HomeAddress);
                Console.WriteLine(student.BirthYear);
                Console.WriteLine(student.SchoolFinished);
            }
        }
    }
}
